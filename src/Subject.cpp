#include "Subject.h"
#include "UniversityDataBase.h"

Subject::Subject(char *_name, unsigned int _departmentId) {
    name = _name;
    departmentId = _departmentId;
}

Subject::Subject(unsigned int _entityId, char *_name, unsigned int _departmentId) {
    entityId = _entityId;
    name = _name;
    departmentId = _departmentId;
}

Subject *Subject::readEntity(ifstream& stream) {
    unsigned int entityId = readEntityId(stream);

    auto lengthString = new char[4];
    unsigned int length;

    stream.readsome(lengthString, UINT_SIZE);
    length = static_cast<unsigned int>(stoi(lengthString));
    auto name = new char[length];
    stream.readsome(name, length);

    auto  departmentIdString = new char[10];
    stream.readsome(departmentIdString, UINT_SIZE);
    auto departmentId = static_cast<unsigned int>(stoi(departmentIdString));

    return new Subject(entityId, name, departmentId);
}

void Subject::writeEntity(ofstream& stream) {
    auto typeId = static_cast<unsigned int>(UniversityEntities::SUBJECT);
    stream.write(reinterpret_cast<char*>(&typeId), UINT_SHORT_SIZE);

    this->writeEntityId(stream);

    const char *nameLength = std::to_string(strlen(this->name)).c_str();

    stream.write(nameLength, UINT_SIZE);
    stream.write(this->name, strlen(this->name));
    stream.write(std::to_string(this->departmentId).c_str(), UINT_SIZE);
}
