#include "Group.h"
#include "UniversityDataBase.h"

Group::Group(char *_name, unsigned int _departmentId) {
    name = _name;
    departmentId = _departmentId;
}

Group::Group(unsigned int _entityId, char *_name, unsigned int _departmentId) {
    entityId = _entityId;
    name = _name;
    departmentId = _departmentId;
}

Group *Group::readEntity(ifstream &stream) {
    unsigned int entityId = readEntityId(stream);

    auto lengthString = new char[4];
    unsigned int length;

    stream.readsome(lengthString, UINT_SIZE);
    length = static_cast<unsigned int>(stoi(lengthString));
    auto name = new char[length];
    stream.readsome(name, length);

    auto departmentIdString = new char[UINT_SIZE];
    stream.readsome(departmentIdString, UINT_SIZE);
    unsigned int departmentId = static_cast<unsigned int>(stoi(departmentIdString));

    return new Group(entityId, name, departmentId);
}

void Group::writeEntity(ofstream& stream) {
    const char *nameLength = std::to_string(strlen(this->name)).c_str();

    stream.write(to_string(UniversityEntities::GROUP).c_str(), UINT_SHORT_SIZE);
    this->writeEntityId(stream);
    stream.write(nameLength, UINT_SIZE);
    stream.write(this->name, strlen(this->name));
    stream.write(std::to_string(sizeof(this->departmentId)).c_str(), UINT_SIZE);
}
