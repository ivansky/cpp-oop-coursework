#ifndef OOPCOURSEWORK_UNIVERSITYDATABASE_H
#define OOPCOURSEWORK_UNIVERSITYDATABASE_H

#include <iostream>
#include <fstream>
#include <vector>
#include "Student.h"
#include "Department.h"
#include "Subject.h"
#include "Group.h"
#include "Estimate.h"

using namespace std;

enum UniversityEntities {
    STUDENT,
    GROUP,
    DEPARTMENT,
    SUBJECT,
    ESTIMATE
};

class UniversityDataBase {
private:
    char* filePath;

    vector<class Student *> students = {};
    vector<class Department *> departments = {};
    vector<class Subject *> subjects = {};
    vector<class Group *> groups = {};
    vector<class Estimate *> estimates = {};

    unsigned int lastId = 0;
public:
    explicit UniversityDataBase(const char *filePath);
    ~UniversityDataBase();

    unsigned int addStudent(class Student*);
    unsigned int addDepartment(class Department*);
    unsigned int addSubject(class Subject*);
    unsigned int addGroup(class Group*);
    unsigned int addEstimate(class Estimate*);

    vector<class Student*> getStudents();
    vector<class Department*> getDepartments();
    vector<class Group*> getGroups();
    vector<class Subject*> getSubjects();
    vector<class Estimate*> getEstimates();
    vector<class Estimate*> getGroupEstimates(unsigned int groupId);

    class Student* findStudent(unsigned int entityId);
    class Department* findDepartment(unsigned int entityId);
    class Group* findGroup(unsigned int entityId);
    class Subject* findSubject(unsigned int entityId);
    class Estimate* findEstimate(unsigned int entityId);

    void removeDepartment(unsigned int entityId);
    void removeStudent(unsigned int entityId);
    void removeGroup(unsigned int entityId);
    void removeSubject(unsigned int entityId);
    void removeEstimate(unsigned int entityId);

    void saveFile();
    void readFile();

    void saveFile(ofstream&);
    void readFile(ifstream&);
};


#endif //OOPCOURSEWORK_UNIVERSITYDATABASE_H
