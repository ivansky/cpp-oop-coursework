#include "Department.h"
#include "UniversityDataBase.h"

Department::Department(char *_name) {
    name = new char[sizeof(_name)];
    strcpy(name, _name);
}

Department::Department(unsigned int _entityId, char *_name) {
    entityId = _entityId;
    name = new char[sizeof(_name)];
    strcpy(name, _name);
}

Department *Department::readEntity(ifstream& stream) {
    unsigned int entityId = readEntityId(stream);

    unsigned int length;

    stream.read(reinterpret_cast<char*>(&length), UINT_SIZE);

    auto name = new char[length];

    stream.read(name, length);

    return new Department(entityId, name);
}

void Department::writeEntity(ofstream& stream) {
    auto nameLength = static_cast<unsigned int>(strlen(this->name));
    auto typeId = static_cast<unsigned int>(UniversityEntities::DEPARTMENT);

    stream.write(reinterpret_cast<char*>(&typeId), UINT_SHORT_SIZE);
    this->writeEntityId(stream);

    char nameLengthBytes[UINT_SIZE];

    copy(reinterpret_cast<const char*>(&nameLength),
         reinterpret_cast<const char*>(&nameLength) + UINT_SIZE,
         nameLengthBytes);

    stream.write(nameLengthBytes, UINT_SIZE);
    stream.write(this->name, nameLength);
}
