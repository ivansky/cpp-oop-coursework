#include "UniversityDataBase.h"
#include "Student.h"
#include "Department.h"
#include "Group.h"
#include "Subject.h"
#include "Estimate.h"

UniversityDataBase::UniversityDataBase(const char *_filePath) : filePath(const_cast<char *>(_filePath)) {}

UniversityDataBase::~UniversityDataBase() {
    //stream.close();
    this->students.clear();
    this->departments.clear();
    this->groups.clear();
    this->subjects.clear();
}

unsigned int UniversityDataBase::addStudent(class Student * student) {
    if(student->entityId == 0) {
        this->lastId++;
        student->setEntityId(this->lastId);
    }
    students.push_back(student);
    return student->entityId;
}

unsigned int UniversityDataBase::addDepartment(class Department * department) {
    if(department->entityId == 0) {
        this->lastId++;
        department->setEntityId(this->lastId);
    }
    departments.push_back(department);
    return department->entityId;
}

unsigned int UniversityDataBase::addSubject(class Subject * subject) {
    if(subject->entityId == 0) {
        this->lastId++;
        subject->setEntityId(this->lastId);
    }
    subjects.push_back(subject);
    return subject->entityId;
}

unsigned int UniversityDataBase::addGroup(class Group * group) {
    if(group->entityId == 0) {
        this->lastId++;
        group->setEntityId(this->lastId);
    }
    groups.push_back(group);
    return group->entityId;
}

unsigned int UniversityDataBase::addEstimate(class Estimate * estimate) {
    if(estimate->entityId == 0) {
        this->lastId++;
        estimate->setEntityId(this->lastId);
    }
    estimates.push_back(estimate);
    return estimate->entityId;
}

vector<class Student*> UniversityDataBase::getStudents() {
    return students;
}

vector<class Department*> UniversityDataBase::getDepartments() {
    return departments;
}

vector<class Group*> UniversityDataBase::getGroups() {
    return groups;
}

vector<class Subject*> UniversityDataBase::getSubjects() {
    return subjects;
}

vector<class Estimate*> UniversityDataBase::getEstimates() {
    return estimates;
}

vector<class Estimate*> UniversityDataBase::getGroupEstimates(unsigned int groupId) {
    vector<class Estimate*> groupEstimates = {};

    for(auto student : students) {
        if (student->groupId != groupId) {
            continue;
        }

        for(auto estimate : estimates) {
            if(estimate->studentId == student->entityId) {
                groupEstimates.push_back(estimate);
            }
        }
    }

    return groupEstimates;
}

class Student* UniversityDataBase::findStudent(unsigned int entityId) {
    for (auto student : students) {
        if (student->entityId == entityId) {
            return student;
        }
    }
    return nullptr;
}

class Department* UniversityDataBase::findDepartment(unsigned int entityId) {
    for (auto department : departments) {
        if (department->entityId == entityId) {
            return department;
        }
    }

    return nullptr;
}

class Group* UniversityDataBase::findGroup(unsigned int entityId) {
    for (auto group : groups) {
        if (group->entityId == entityId) {
            return group;
        }
    }

    return nullptr;
}

class Subject* UniversityDataBase::findSubject(unsigned int entityId) {
    for (auto subject : subjects) {
        if (subject->entityId == entityId) {
            return subject;
        }
    }

    return nullptr;
}

class Estimate* UniversityDataBase::findEstimate(unsigned int entityId) {
    for (auto estimate : estimates) {
        if (estimate->entityId == entityId) {
            return estimate;
        }
    }

    return nullptr;
}

void UniversityDataBase::saveFile() {
    ofstream stream(filePath, ios_base::binary);
    saveFile(stream);
    stream.close();
}

void UniversityDataBase::readFile() {
    ifstream stream(filePath, ios::in | ios_base::binary);

    if (stream.is_open()) {
        readFile(stream);
    } else {
        throw "Can not open file";
    }

    stream.close();
}

void UniversityDataBase::saveFile(ofstream &stream) {
    stream.flush();

    for(auto department : this->getDepartments()) {
        department->writeEntity(stream);
    }

    for(auto group : this->getGroups()) {
        group->writeEntity(stream);
    }

    for(auto student : this->getStudents()) {
        student->writeEntity(stream);
    }

    for(auto subject : this->getSubjects()) {
        subject->writeEntity(stream);
    }

    for(auto estimate : this->getEstimates()) {
        estimate->writeEntity(stream);
    }
}

void UniversityDataBase::readFile(ifstream& stream) {
    stream.seekg (0, ios::beg);

    unsigned int typeId;
    unsigned int currentId = 0;

    while (!stream.eof() && stream.read(reinterpret_cast<char*>(&typeId), UINT_SHORT_SIZE)) {

        switch(typeId) {
            case UniversityEntities::STUDENT:
                currentId = addStudent(Student::readEntity(stream));
                break;
            case UniversityEntities::GROUP:
                currentId = addGroup(Group::readEntity(stream));
                break;
            case UniversityEntities::DEPARTMENT:
                currentId = addDepartment(Department::readEntity(stream));
                break;
            case UniversityEntities::SUBJECT:
                currentId = addSubject(Subject::readEntity(stream));
                break;
            case UniversityEntities::ESTIMATE:
                currentId = addEstimate(Estimate::readEntity(stream));
                break;
            default:break;
        }

        if (this->lastId < currentId) {
            this->lastId = currentId;
        }
    }
}

void UniversityDataBase::removeDepartment(unsigned int entityId) {
    for(auto department : departments) {
        if (department->entityId == entityId) {
            departments.erase(remove(departments.begin(), departments.end(), department), departments.end());
            break;
        }
    }

    for(auto subject : subjects) {
        if(subject->departmentId == entityId) {
            this->removeSubject(subject->entityId);
        }
    }

    for(auto group : groups) {
        if(group->departmentId == entityId) {
            this->removeGroup(group->entityId);
        }
    }
}

void UniversityDataBase::removeStudent(unsigned int entityId) {
    for(auto student : students) {
        if (student->entityId == entityId) {
            students.erase(remove(students.begin(), students.end(), student), students.end());
            break;
        }
    }

    for(auto estimate : estimates) {
        if(estimate->studentId == entityId) {
            this->removeEstimate(estimate->entityId);
        }
    }
}

void UniversityDataBase::removeGroup(unsigned int entityId) {
    for(auto group : groups) {
        if (group->entityId == entityId) {
            groups.erase(remove(groups.begin(), groups.end(), group), groups.end());
            break;
        }
    }

    for(auto student : students) {
        if(student->groupId == entityId) {
            this->removeStudent(student->entityId);
        }
    }
}

void UniversityDataBase::removeSubject(unsigned int entityId) {
    for(auto subject : subjects) {
        if (subject->entityId == entityId) {
            subjects.erase(remove(subjects.begin(), subjects.end(), subject), subjects.end());
            break;
        }
    }

    for(auto estimate : estimates) {
        if(estimate->subjectId == entityId) {
            this->removeEstimate(estimate->entityId);
        }
    }
}

void UniversityDataBase::removeEstimate(unsigned int entityId) {
    for(auto estimate : estimates) {
        if (estimate->entityId == entityId) {
            estimates.erase(remove(estimates.begin(), estimates.end(), estimate), estimates.end());
            break;
        }
    }
}
