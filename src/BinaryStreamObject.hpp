#ifndef OOPCOURSEWORK_BINARYSTREAMOBJECT_H
#define OOPCOURSEWORK_BINARYSTREAMOBJECT_H

#include <iostream>
#include <fstream>
#include <string>

#ifndef UINT_SHORT_SIZE
#define UINT_SHORT_SIZE 4
#endif

#ifndef UINT_SIZE
#define UINT_SIZE 8
#endif

using namespace std;

template <class T>
class BinaryStreamObject {
public:
    unsigned int entityId = 0;

    static unsigned int readEntityId(ifstream&);
    void writeEntityId(ofstream&);
    void setEntityId(unsigned int id);

    static T* readEntity(ifstream&);
    virtual void writeEntity(ofstream&) = 0;

};

template<class T>
unsigned int BinaryStreamObject<T>::readEntityId(ifstream& stream) {
    unsigned int entityId;
    stream.read(reinterpret_cast<char*>(&entityId), UINT_SIZE);
    return entityId;
}

template<class T>
void BinaryStreamObject<T>::writeEntityId(ofstream& stream) {
    stream.write(reinterpret_cast<char*>(&this->entityId), UINT_SIZE);
}

template<class T>
void BinaryStreamObject<T>::setEntityId(unsigned int id) {
    this->entityId = id;
}

#endif //OOPCOURSEWORK_BINARYSTREAMOBJECT_H
