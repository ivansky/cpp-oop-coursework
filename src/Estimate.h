#ifndef OOPCOURSEWORK_ESTIMATE_H
#define OOPCOURSEWORK_ESTIMATE_H

#include "BinaryStreamObject.hpp"

class Estimate : public BinaryStreamObject<Estimate> {
public:
    unsigned int studentId;
    unsigned int subjectId;
    unsigned int value;

    Estimate(unsigned int studentId, unsigned int subjectId, unsigned int value);
    Estimate(unsigned int entityId, unsigned int studentId, unsigned int subjectId, unsigned int value);

    static Estimate* readEntity(ifstream&);
    void writeEntity(ofstream&) override;
};


#endif //OOPCOURSEWORK_ESTIMATE_H
