#ifndef OOPCOURSEWORK_DEPARTMENT_H
#define OOPCOURSEWORK_DEPARTMENT_H


#include "BinaryStreamObject.hpp"
#include <iostream>
#include <fstream>

using namespace std;

class Department : public BinaryStreamObject<class Department> {
public:
    char* name;
    explicit Department(char* name);
    Department(unsigned int entityId, char* name);
    static Department* readEntity(ifstream&);

    void writeEntity(ofstream &stream) override;
};


#endif //OOPCOURSEWORK_DEPARTMENT_H
