#ifndef OOPCOURSEWORK_SUBJECT_H
#define OOPCOURSEWORK_SUBJECT_H


#include "BinaryStreamObject.hpp"
#include <iostream>
#include <fstream>

using namespace std;

class Subject : public BinaryStreamObject<Subject> {
public:
    char* name;
    unsigned int departmentId;

    Subject(char *name, unsigned int departmentId);
    Subject(unsigned int entityId,
            char* name,
            unsigned int departmentId);
    static Subject* readEntity(ifstream&);
    void writeEntity(ofstream&) override;
};


#endif
