#ifndef OOPCOURSEWORK_STUDENT_H
#define OOPCOURSEWORK_STUDENT_H


#include "BinaryStreamObject.hpp"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class Student : public BinaryStreamObject<Student> {
public:
    char* firstName;
    char* lastName;
    unsigned int groupId;

    Student(char *firstName,
            char *lastName,
            unsigned int groupId);

    Student(unsigned int entityId,
            char* firstName,
            char* lastName,
            unsigned int groupId);
    static Student* readEntity(ifstream&);
    void writeEntity(ofstream&) override;
};


#endif //OOPCOURSEWORK_STUDENT_H
