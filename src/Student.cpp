#include "Student.h"
#include "UniversityDataBase.h"

Student::Student(char *_firstName,
                 char *_lastName,
                 unsigned int _groupId) {
    firstName = new char[strlen(_firstName)];
    lastName = new char[strlen(_lastName)];
    strcpy(firstName, _firstName);
    strcpy(lastName, _lastName);
    groupId = _groupId;
}

Student::Student(unsigned int _entityId,
                 char *_firstName,
                 char *_lastName,
                 unsigned int _groupId) {
    entityId = _entityId;
    firstName = new char[strlen(_firstName)];
    lastName = new char[strlen(_lastName)];
    strcpy(firstName, _firstName);
    strcpy(lastName, _lastName);
    groupId = _groupId;
}

Student *Student::readEntity(ifstream& stream) {
    unsigned int entityId = readEntityId(stream);

    auto lengthString = new char[4];
    unsigned int length;

    stream.readsome(lengthString, UINT_SIZE);
    length = static_cast<unsigned int>(stoi(lengthString));
    auto firstName = new char[length];
    stream.readsome(firstName, length);

    stream.readsome(lengthString, UINT_SIZE);
    length = static_cast<unsigned int>(stoi(lengthString));
    auto secondName = new char[length];
    stream.readsome(secondName, length);

    auto groupIdString = new char[UINT_SIZE];
    stream.readsome(groupIdString, UINT_SIZE);
    unsigned int groupEntityId = static_cast<unsigned int>(stoi(groupIdString));

    return new Student(entityId, firstName, secondName, groupEntityId);
}

void Student::writeEntity(ofstream& stream) {
    const char *firstNameLength = std::to_string(strlen(this->firstName)).c_str();
    const char *lastNameLength = std::to_string(strlen(this->lastName)).c_str();

    stream.write(to_string(UniversityEntities::STUDENT).c_str(), UINT_SHORT_SIZE);
    this->writeEntityId(stream);
    stream.write(firstNameLength, UINT_SIZE);
    stream.write(this->firstName, strlen(this->firstName));
    stream.write(lastNameLength, UINT_SIZE);
    stream.write(this->lastName, strlen(this->lastName));
    stream.write(std::to_string(this->groupId).c_str(), UINT_SIZE);
}
