#ifndef OOPCOURSEWORK_GROUP_H
#define OOPCOURSEWORK_GROUP_H

#include "BinaryStreamObject.hpp"
#include <iostream>
#include <fstream>

using namespace std;

class Group : public BinaryStreamObject<Group> {
public:
    char* name;
    unsigned int departmentId;

    Group(char *name, unsigned int departmentId);
    Group(unsigned int entityId,
          char* groupName,
          unsigned int departmentId);

    static Group* readEntity(ifstream& stream);
    void writeEntity(ofstream& stream) override;
};


#endif //OOPCOURSEWORK_GROUP_H
