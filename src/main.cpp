#include <iostream>
#include <stdio.h>
#include "UniversityDataBase.h"

using namespace std;

void PressEnterToContinue();
void clearScreen();
void departmentsMenu(UniversityDataBase* university);
void subjectsMenu(UniversityDataBase* university);
void studentsMenu(UniversityDataBase* university);
void groupsMenu(UniversityDataBase* university);
void estimatesMenu(UniversityDataBase* university);

int main(int argc, const char * argv[]) {
    char select;

    const char* FILE_PATH = "database.bin";

    auto university = new UniversityDataBase(FILE_PATH);

    university->readFile();

    while (true)
    {
        clearScreen();
        cout << "Choose one of the following operations:\n\n";
        cout << "1. Departments;\n";
        cout << "2. Subjects;\n";
        cout << "3. Groups;\n";
        cout << "4. Students;\n";
        cout << "5. Estimates;\n";
        cout << "\n";
        cout << "5. - Report estimates for group;\n";
        cout << "\n";

        cout << "s - Save database.\n\n>> ";
        cout << "q - Quit.\n\n>> ";

        cin >> select;

        switch(select)
        {
            case '1': {
                departmentsMenu(university);
                break;
            }
            case '2': {
                subjectsMenu(university);
                break;
            }
            case '3': {
                groupsMenu(university);
                break;
            }
            case '4': {
                studentsMenu(university);
                break;
            }
            case '5': {
                estimatesMenu(university);
                break;
            }
            case 's': {
                university->saveFile();
                cout << "File was saved." << endl;
                break;
            }
            case 'q': {
                return 0;
            }
            default: {
                cout << "Wrong option." << endl;
                break;
            }
        }

        PressEnterToContinue();
    }
}

void departmentsMenu(UniversityDataBase* university) {
    char select;

    while (true)
    {
        clearScreen();
        cout << "Departments:\n\n";
        cout << "1. List;\n";
        cout << "2. Add;\n";
        cout << "3. Remove;\n";
        cout << "4. <- Back;\n";
        cout << "\n";

        cin >> select;

        switch(select) {
            case '1': {
                if (university->getDepartments().empty()) {
                    cout << "Departments are not found." << endl;
                } else {
                    cout << "Departments:" << endl;
                    for (auto department : university->getDepartments()) {
                        cout << "[" << department->entityId << "] " << department->name << ";" << endl;
                    }
                }

                break;
            }
            case '2': {
                cout << "Add Department" << endl;
                cout << "Enter department name: ";

                char* departmentName = new char;
                cin >> departmentName;

                auto department = new class Department(departmentName);
                university->addDepartment(department);

                cout << "Department was successfully added." << endl;

                break;
            }
            case '3': {
                cout << "Remove Department:" << endl;

                if (university->getDepartments().empty()) {
                    cout << "Departments are not found." << endl;
                    break;
                }

                unsigned int departmentId;

                cout << "Choose department from next list: " << endl;

                for (auto department : university->getDepartments()) {
                    cout << "[" << department->entityId << "] " << department->name << ";" << endl;
                }

                while(true) {
                    cout << "Press ID of department for subject: ";
                    cin >> departmentId;

                    if (university->findDepartment(departmentId) == nullptr) {
                        cout << "Department [" << departmentId << "] has not found." << endl;
                    } else {
                        auto name = university->findDepartment(departmentId)->name;
                        university->removeDepartment(departmentId);
                        cout << "Department `" << name << "` was successfully removed." << endl;
                        break;
                    }
                }

                break;
            }
            default: {
                goto skip_first_loop;
            }
        }

        PressEnterToContinue();
    }

    skip_first_loop: cout << "";
}

void subjectsMenu(UniversityDataBase* university) {
    char select;

    while (true) {
        clearScreen();
        cout << "Subjects:\n\n";
        cout << "1. List;\n";
        cout << "2. Add;\n";
        cout << "3. Remove;\n";
        cout << "4. <- Back;\n";
        cout << "\n";

        cin >> select;

        switch (select) {
            case '1': {
                if (university->getSubjects().empty()) {
                    cout << "Subjects are not found." << endl;
                } else {
                    cout << "Subjects:" << endl;
                    for (auto subject : university->getSubjects()) {
                        cout << "[" << subject->entityId << "] " << subject->name << ";" << endl;
                    }
                }
                break;
            }
            case '2': {
                cout << "Add Subject" << endl;

                if (university->getDepartments().empty()) {
                    cout << "Departments are not found." << endl;
                    break;
                }

                cout << "Enter subject name: ";

                char* subjectName = new char;
                unsigned int departmentId;
                cin >> subjectName;

                cout << "Choose department from next list: " << endl;

                for (auto department : university->getDepartments()) {
                    cout << "[" << department->entityId << "] " << department->name << ";" << endl;
                }

                cout << "And press ID of department for subject: ";
                cin >> departmentId;

                while (university->findDepartment(departmentId) == nullptr) {
                    cout << "Did not find department with ID(" << departmentId << ")." << endl;
                    cout << "Please press existing department ID: ";
                    cin >> departmentId;
                }

                auto subject = new class Subject(subjectName, departmentId);

                university->addSubject(subject);

                cout << "Subject was successfully added." << endl;

                break;
            }
            case '3': {
                cout << "Remove Subject" << endl;

                if (university->getSubjects().empty()) {
                    cout << "Subjects are not found." << endl;
                    break;
                }

                unsigned int subjectId;

                cout << "Choose subject from next list: " << endl;

                for (auto subject : university->getSubjects()) {
                    cout << "[" << subject->entityId << "] " << subject->name << ";" << endl;
                }

                while(true) {
                    cout << "Press ID of subject for subject: ";
                    cin >> subjectId;

                    if (university->findSubject(subjectId) == nullptr) {
                        cout << "Subject [" << subjectId << "] has not found." << endl;
                    } else {
                        auto name = university->findSubject(subjectId)->name;
                        university->removeSubject(subjectId);
                        cout << "Subject `" << name << "` was successfully removed." << endl;
                        break;
                    }
                }

                break;
            }
            default: {
                goto skip_second_loop;
            }
        }
        PressEnterToContinue();
    }

    skip_second_loop: cout << "";
}

void studentsMenu(UniversityDataBase* university) {
    char select;

    while (true) {
        clearScreen();
        cout << "Students:\n\n";
        cout << "1. List;\n";
        cout << "2. Add;\n";
        cout << "3. Remove;\n";
        cout << "4. <- Back;\n";
        cout << "\n";

        cin >> select;

        switch (select) {
            case '1': {
                if (university->getStudents().empty()) {
                    cout << "Students are not found." << endl;
                } else {
                    cout << "Students:" << endl;
                    for (auto student : university->getStudents()) {
                        cout << "[" << student->entityId << "] ";
                        cout << student->firstName << " ";
                        cout << student->lastName << ";" << endl;
                    }
                }
                break;
            }
            case '2': {
                cout << "Add Student" << endl;

                if (university->getGroups().empty()) {
                    cout << "Groups are not found." << endl;
                    cout << "You should add a group at first." << endl;
                    break;
                }

                char* firstName = new char;
                char* lastName = new char;
                unsigned int groupId;

                cout << "Enter First name: ";
                cin >> firstName;

                cout << "Enter Last name: ";
                cin >> lastName;

                cout << "Choose group from the next list: " << endl;

                for (auto group : university->getGroups()) {
                    cout << "[" << group->entityId << "] " << group->name << ";" << endl;
                }

                cout << "And press ID of group for student: ";
                cin >> groupId;

                while (university->findGroup(groupId) == nullptr) {
                    cout << "Did not find group with ID(" << groupId << ")." << endl;
                    cout << "Please press existing group ID: ";
                    cin >> groupId;
                }

                auto student = new class Student(firstName, lastName, groupId);

                university->addStudent(student);

                cout << "Student was successfully added." << endl;

                break;
            }
            case '3': {
                cout << "Remove Student" << endl;

                if (university->getStudents().empty()) {
                    cout << "Students are not found." << endl;
                    break;
                }

                unsigned int studentId;

                cout << "Choose student from next list: " << endl;

                for (auto student : university->getStudents()) {
                    cout << "[" << student->entityId << "] ";
                    cout << student->firstName << " " << student->lastName << ";" << endl;
                }

                while(true) {
                    cout << "Press ID of student for removing: ";
                    cin >> studentId;

                    if (university->findStudent(studentId) == nullptr) {
                        cout << "Student [" << studentId << "] has not found." << endl;
                    } else {
                        char* name = new char;

                        strcpy(name, university->findStudent(studentId)->firstName);
                        strcat(name, " ");
                        strcat(name, university->findStudent(studentId)->lastName);

                        university->removeStudent(studentId);
                        cout << "Student `" << name << "` was successfully removed." << endl;
                        break;
                    }
                }

                break;
            }
            default: {
                goto skip_fours_loop;
            }
        }

        PressEnterToContinue();
    }

    skip_fours_loop: cout << "";
}

void groupsMenu(UniversityDataBase* university) {
    char select;

    while (true) {
        clearScreen();
        cout << "Groups:\n\n";
        cout << "1. List;\n";
        cout << "2. Add;\n";
        cout << "3. Remove;\n";
        cout << "4. <- Back;\n";
        cout << "\n";

        cin >> select;

        switch (select) {
            case '1': {
                if (university->getGroups().empty()) {
                    cout << "Groups are not found." << endl;
                } else {
                    cout << "Groups:" << endl;
                    for (auto group : university->getGroups()) {
                        cout << "[" << group->entityId << "] " << group->name << ";" << endl;
                    }
                }

                break;
            }
            case '2': {
                cout << "Add Group" << endl;

                if (university->getDepartments().empty()) {
                    cout << "Departments are not found. You can not add group." << endl;
                    break;
                }

                cout << "Enter group name: ";

                char* groupName = new char;
                unsigned int departmentId;
                cin >> groupName;

                cout << "Choose department from next list: " << endl;

                for (auto department : university->getDepartments()) {
                    cout << "[" << department->entityId << "] " << department->name << ";" << endl;
                }

                cout << "And press ID of department for group: ";
                cin >> departmentId;

                while (university->findDepartment(departmentId) == nullptr) {
                    cout << "Did not find department with ID(" << departmentId << ")." << endl;
                    cout << "Please press existing department ID: ";
                    cin >> departmentId;
                }

                auto group = new class Group(groupName, departmentId);

                university->addGroup(group);

                cout << "Group was successfully added." << endl;

                break;
            }
            case '3': {
                cout << "Remove Group" << endl;

                if (university->getGroups().empty()) {
                    cout << "Groups are not found." << endl;
                    break;
                }

                unsigned int groupId;

                cout << "Choose group from next list: " << endl;

                for (auto group : university->getGroups()) {
                    cout << "[" << group->entityId << "] " << group->name << ";" << endl;
                }

                while(true) {
                    cout << "Press ID of group for group: ";
                    cin >> groupId;

                    if (university->findGroup(groupId) == nullptr) {
                        cout << "Group [" << groupId << "] has not found." << endl;
                    } else {
                        auto name = university->findGroup(groupId)->name;
                        university->removeGroup(groupId);
                        cout << "Group `" << name << "` was successfully removed." << endl;
                        break;
                    }
                }

                break;
            }
            default: {
                goto skip_third_loop;
            }
        }

        PressEnterToContinue();
    }

    skip_third_loop: cout << "";
}

void estimatesMenu(UniversityDataBase* university) {
    char select;

    while (true) {
        clearScreen();
        cout << "Estimates:\n\n";
        cout << "1. List;\n";
        cout << "2. Add;\n";
        cout << "3. Remove;\n";
        cout << "4. <- Back;\n";
        cout << "\n";

        cin >> select;

        switch (select) {
            case '1': {
                if (university->getEstimates().empty()) {
                    cout << "Estimates are not found." << endl;
                } else {
                    cout << "Estimates:" << endl;
                    for (auto estimate : university->getEstimates()) {
                        auto student = university->findStudent(estimate->studentId);
                        auto subject = university->findSubject(estimate->subjectId);

                        char* name = new char;
                        strcpy(name, student->firstName);
                        strcat(name, student->lastName);

                        cout << "[" << estimate->entityId << "] ";
                        cout << "`" << name << "` ";
                        cout << "in `" << subject->name << "` ";
                        cout << "has " << estimate->value << " grade;" << endl;
                    }
                }
                break;
            }
            case '2': {
                cout << "Add Estimate" << endl;

                int relationsExists = 1;

                if (university->getSubjects().empty()) {
                    cout << "Subjects are not found." << endl;
                    cout << "You should add a subject at first." << endl;
                    relationsExists = 0;
                }

                if (university->getStudents().empty()) {
                    cout << "Students are not found." << endl;
                    cout << "You should add a student at first." << endl;
                    relationsExists = 0;
                }

                if (relationsExists == 0) {
                    break;
                }

                auto studentName = new char;
                auto subjectName = new char;

                cout << "To add estimate choose student from next list: " << endl;

                for (auto student : university->getStudents()) {
                    cout << "[" << student->entityId << "] ";
                    cout << student->firstName << " " << student->lastName << ";" << endl;
                }

                unsigned int studentId;

                while(true) {
                    cout << "Press ID of student for estimate: ";
                    cin >> studentId;

                    if (university->findStudent(studentId) == nullptr) {
                        cout << "Student [" << studentId << "] has not found." << endl;
                    } else {

                        strcpy(studentName, university->findStudent(studentId)->firstName);
                        strcat(studentName, " ");
                        strcat(studentName, university->findStudent(studentId)->lastName);

                        cout << "You have picked Student: `" << studentName << "`." << endl;
                        break;
                    }
                }

                cout << "Choose Subject from next list: " << endl;

                for (auto subject : university->getSubjects()) {
                    cout << "[" << subject->entityId << "] ";
                    cout << subject->name << ";" << endl;
                }

                unsigned int subjectId;

                while(true) {
                    cout << "Press ID of subject for estimate: ";
                    cin >> subjectId;

                    if (university->findSubject(subjectId) == nullptr) {
                        cout << "Subject [" << subjectId << "] has not found." << endl;
                    } else {
                        strcpy(subjectName, university->findSubject(subjectId)->name);

                        cout << "You have picked Subject `" << subjectName << "`." << endl;
                        break;
                    }
                }

                unsigned int value = 0;

                cout << "Estimate `" << studentName << "` for `" << subjectName << "`";
                cout << "and ";

                while (true) {
                    cout << "press grade (1 - 5):";
                    cin >> value;

                    if (value >= 1 && value <= 5) {
                        break;
                    } else {
                        cout << " < wrong value." << endl;
                    }
                }

                auto estimate = new class Estimate(studentId, subjectId, value);

                university->addEstimate(estimate);

                cout << "Estimate was successfully added." << endl;

                break;
            }
            case '3': {
                cout << "Remove Estimate" << endl;

                if (university->getGroups().empty()) {
                    cout << "Groups are not found." << endl;
                } else {
                    cout << "Groups:" << endl;
                    for (auto group : university->getGroups()) {
                        cout << "[" << group->entityId << "] " << group->name << ";" << endl;
                    }
                }

                unsigned int groupId;

                cout << "Press ID of group where do you want delete Estimation: ";
                cin >> groupId;


                auto group = university->findGroup(groupId);

                if (group == nullptr) {
                    cout << "Group [" << groupId << "] is not found." << endl;
                    break;
                }

                auto groupEstimates = university->getGroupEstimates(groupId);

                if (groupEstimates.empty()) {
                    cout << "Estimates or this group are not found." << endl;
                    break;
                }

                cout << "Estimates:" << endl;
                for (auto estimate : groupEstimates) {
                    auto student = university->findStudent(estimate->studentId);
                    auto subject = university->findSubject(estimate->subjectId);

                    char* name = new char;
                    strcpy(name, student->firstName);
                    strcat(name, student->lastName);

                    cout << "[" << estimate->entityId << "] ";
                    cout << "`" << name << "` ";
                    cout << "in `" << subject->name << "` ";
                    cout << "has " << estimate->value << " grade;" << endl;
                }

                unsigned int estimateId;
                cout << "Press ID of Estimation to remove: ";
                cin >> estimateId;

                university->removeEstimate(groupId);
                cout << "Estimation was successfully removed." << endl;

                break;
            }
            default: {
                goto skip_fives_loop;
            }
        }

        PressEnterToContinue();
    }

    skip_fives_loop: cout << "";
}

void PressEnterToContinue()
{
    getchar(); // waiting for Enter
    int c;
    printf("\nPress ENTER to continue... ");
    fflush(stdin);
    fflush(stdout);
    do c = getchar(); while ((c != '\n') && (c != EOF));
}

void clearScreen()
{
#if defined(_WIN32) || defined(WIN32)
    std::system("cls");
#else
    // Assume POSIX
    std::system("clear");
#endif
}