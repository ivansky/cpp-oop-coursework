#include "Estimate.h"
#include "UniversityDataBase.h"

Estimate::Estimate(unsigned int _studentId, unsigned int _subjectId, unsigned int _value) {
    studentId = _studentId;
    subjectId = _subjectId;
    value = _value;
}

Estimate::Estimate(unsigned int _entityId, unsigned int _studentId, unsigned int _subjectId, unsigned int _value) {
    entityId = _entityId;
    studentId = _studentId;
    subjectId = _subjectId;
    value = _value;
}

Estimate *Estimate::readEntity(ifstream& stream) {
    unsigned int entityId = readEntityId(stream);

    auto studentIdString = new char[UINT_SIZE];
    unsigned int studentId;
    stream.readsome(studentIdString, UINT_SIZE);
    studentId = static_cast<unsigned int>(stoi(studentIdString));

    auto subjectIdString = new char[UINT_SIZE];
    unsigned int subjectId;
    stream.readsome(subjectIdString, UINT_SIZE);
    subjectId = static_cast<unsigned int>(stoi(subjectIdString));

    auto valueString = new char[UINT_SIZE];
    unsigned int value;
    stream.readsome(valueString, UINT_SIZE);
    value = static_cast<unsigned int>(stoi(valueString));

    return new Estimate(entityId, studentId, subjectId, value);
}

void Estimate::writeEntity(ofstream& stream) {
    stream.write(to_string(UniversityEntities::ESTIMATE).c_str(), UINT_SHORT_SIZE);
    this->writeEntityId(stream);
    stream.write(to_string(this->studentId).c_str(), UINT_SIZE);
    stream.write(to_string(this->subjectId).c_str(), UINT_SIZE);
    stream.write(to_string(this->value).c_str(), UINT_SIZE);
}
