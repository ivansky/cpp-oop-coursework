# OOP Coursework
Система отслеживания успеваемости студентов

## Necessary tools
- [**CMake**](https://cmake.org/download/)
- Compiler *(one of them)*
    * [**GCC 6 +**](https://gcc.gnu.org/install/binaries.html)
    * [**CLang 3.5 +**](http://releases.llvm.org/download.html)
    * [**MSVC 19 +**](http://landinghub.visualstudio.com/visual-cpp-build-tools)

## Build
Windows Cmd

`$ ./build.bat`

Unix / MacOS terminal

`$ chmod +x ./build.sh && ./build.sh`


## Run
Windows Cmd

`$ ./main.exe`

Unix / MacOS terminal

`$ ./main`

## Task

Система отслеживания успеваемости студентов

Система ведения результатов успеваемости студентов.

Ведение БД: 
* кафедры
* студенты 
* предметы

Функции: 
* занесение данных по студентам
* формирование справочных документов

Выходные документы: 
* Ведомость успеваемости по группе студентов

## Author

© Ivan Martianov
